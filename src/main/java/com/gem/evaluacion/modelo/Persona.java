/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gem.evaluacion.modelo;

import com.google.gson.annotations.SerializedName;
import java.util.Date;

/**
 *
 * @author darwin
 */
public class Persona {
    String origen,nombres, apellidos, cedula, correo;
    String telefono, pais;
    @SerializedName("fechaNacimiento")
    Date fechaNacimiento;
    int edad;
    
    public Persona(){}

    public Persona(String origen,String nombres,String apellidos, String cedula, String correo, String telefono, String pais,Date fechaNacimiento,int edad){
        this.origen=origen;
        this.nombres=nombres;
        this.apellidos=apellidos;
        this.cedula=cedula;
        this.correo=correo;        
        this.telefono=telefono;
        this.pais=pais;
        this.fechaNacimiento=fechaNacimiento;
        this.edad=edad;
        
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
            
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

   
    
}
