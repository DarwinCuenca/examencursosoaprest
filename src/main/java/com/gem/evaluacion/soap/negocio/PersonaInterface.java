/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gem.evaluacion.soap.negocio;

import com.gem.evaluacion.modelo.Persona;
import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author darwin
 */

@WebService
public interface PersonaInterface {
    @WebMethod
    public List<Persona>findAll();
}
