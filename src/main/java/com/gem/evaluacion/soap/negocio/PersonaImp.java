/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gem.evaluacion.soap.negocio;

import com.gem.evaluacion.modelo.Persona;
import com.gem.evaluacion.rest.negocio.PersonaService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;

/**
 *
 * @author darwin
 */
@WebService(endpointInterface = "com.gem.evaluacion.soap.negocio.PersonaInterface")
public class PersonaImp implements PersonaInterface{
    
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public List<Persona> findAll() {
        return rellenarInforPrueba();
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private List<Persona> rellenarInforPrueba() {
        List<Persona>personas=new ArrayList<>();
        try {
            Persona p1=new Persona("SOAP","Leonel", "Messi", "123456789", "messil@fedeFutbol.ar", "123456", "Argentina", format.parse("10/01/1990"), 28);
            Persona p2=new Persona("SOAP","Marco", "Echeverry", "987520653", "echeverryM@fedeFutbol.bo", "789456", "Bolivia", format.parse("10/05/1991"), 27);
            Persona p3=new Persona("SOAP","Radamel", "Falcao", "213658964", "falcaor@fedeFutbol.co", "852200", "Colombia", format.parse("25/03/1988"), 30);
            Persona p4=new Persona("SOAP","Antonio", "Valencia", "111125803", "valenciaa@fedeFutbol.ec", "852200", "Ecuador", format.parse("28/02/1989"), 31);
            Persona p5=new Persona("SOAP","Paolo", "Guerrero", "36366598", "guerrerop@fedeFutbol.pe", "320052", "Peru", format.parse("02/09/1990"), 28);
            Persona p6=new Persona("SOAP","Salomon", "Rondon", "8885552012", "rondons@fedeFutbol.ve", "321654", "Venezuela", format.parse("30/11/1992"), 26);
            personas.add(p1);
            personas.add(p2);
            personas.add(p3);
            personas.add(p4);
            personas.add(p5);
            personas.add(p6);
        } catch (ParseException ex) {
            Logger.getLogger(PersonaService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return personas;
    }    
}
