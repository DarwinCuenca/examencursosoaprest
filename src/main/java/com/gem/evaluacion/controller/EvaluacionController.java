/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gem.evaluacion.controller;

import com.gem.evaluacion.modelo.Persona;
import com.gem.evaluacion.soap.negocio.PersonaImp;
import com.gem.evaluacion.soap.negocio.PersonaImpService;
import com.gem.evaluacion.soap.negocio.PersonaInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.omnifaces.cdi.ViewScoped;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.xml.ws.Endpoint;

/**
 *
 * @author darwin
 */
@Named
@ViewScoped
public class EvaluacionController implements Serializable {
    
    boolean mostrarFecha;

    public boolean isMostrarFecha() {
        return mostrarFecha;
    }

    public void setMostrarFecha(boolean mostrarFecha) {
        this.mostrarFecha = mostrarFecha;
    }

    List<Persona> personas = new ArrayList();

    public List<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(List<Persona> personas) {
        this.personas = personas;
    }

    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

    public void obtenerporREST() throws ParseException {
        mostrarFecha=false;
        personas = new ArrayList<>();
        System.out.println("EVALUACION REST");
        Client client = ClientBuilder.newClient();
        
        try {

            URL url = new URL("http://127.0.0.1:8086/evaluacionSOAP_REST/resources/CursoPersonaService/findAll");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");

            while ((output = br.readLine()) != null) {
                System.out.println(output);
                System.out.println(output);
                try {
                    //Gson gson = new Gson();
                    Gson gson=new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
                    List<Persona> listPersonas = gson.fromJson(output, new TypeToken<List<Persona>>() {
                    }.getType());
                    if (listPersonas != null) {
                        for (Persona object : listPersonas) {
                            System.out.println("\nOrigen : " + object.getOrigen() + " " + object.getNombres() + " "+object.getApellidos());
                        }
                    }
                    personas=listPersonas;
                } catch (JsonSyntaxException e) {
                    System.err.println("JsonSyntaxException: " + e.getMessage());
                }

            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

    }

    public void obtenerporSOAP() {
        mostrarFecha=true;
        System.out.println("EVALUACION SOAP");
        personas = new ArrayList<>();
        Endpoint.publish("http://localhost:1515/WS/findAllPersona", new PersonaImp());

        PersonaImpService personaSOAPService = new PersonaImpService();
        PersonaInterface consumer = personaSOAPService.getPersonaImpPort();
        personas = consumer.findAll();
    }
}
