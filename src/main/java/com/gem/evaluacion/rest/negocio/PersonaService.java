/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gem.evaluacion.rest.negocio;

import com.gem.evaluacion.modelo.Persona;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author darwin
 */
@Path("/CursoPersonaService")
@ApplicationPath("/resources")
public class PersonaService extends Application {
    
    //@Inject
    //CrudServiceAgrocatsa em;    
    
    @GET
    @Path("/findAll")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Persona> findAll() {
        List<Persona> result = rellenarInforPrueba();
        return result;
    }    

    
    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    
    private List<Persona> rellenarInforPrueba() {
        List<Persona>personas=new ArrayList<>();
        
            Persona p1=new Persona("REST","Mauricio", "Macri", "123456789", "macrim@presidencia.ar", "123456", "Argentina", null, 68);
            Persona p2=new Persona("REST","Juan Evo", "Morales Ayma", "987520653", "moralesm@presidencia.bo", "789456", "Bolivia", null, 58);
            Persona p3=new Persona("REST","Juan Manuel", "Santos Calderon", "213658964", "santosj@presidencia.co", "852200", "Colombia", null, 58);
            Persona p4=new Persona("REST","Lenin Boltaire", "Moreno Gaces", "111125803", "morenol@presidencia.ec", "852200", "Ecuador", null, 59);
            Persona p5=new Persona("REST","Pedro Pablo", "Kuczynski", "36366598", "Kuczynski@presidencia.pe", "320052", "Peru", null, 49);
            Persona p6=new Persona("REST","Nicolas", "Maduro Moros", "8885552012", "maduron@presidencia.ve", "321654", "Venezuela", null, 49);
            personas.add(p1);
            personas.add(p2);
            personas.add(p3);
            personas.add(p4);
            personas.add(p5);
            personas.add(p6);


        return personas;
    }
}
